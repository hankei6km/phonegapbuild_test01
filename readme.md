PhoneGapBuildのテスト用
====================

PhoneGapBuildからBitbucketのGitリポジトリがPullできるかの実験と、PhoneGapBuildのデバッグ機能を試すためのリポジトリ。

ビルド
----

このリポジトリをそのまま使うなら、PhoneGapBuild上でアプリを作成するときに 「pull from a git/svn repo url」を選択し、URLに `https://bitbucket.org/hankei6km/phonegapbuild_test01.git` を指定。

デバッグ
-----

- PhoneGapBuild上で作成したアプリに対して、アプリの設定画面かから「enable debug mode」を有効にする
- ビルドされたパッケージをダウンロードしてAndroidやwebOSのエミュレータなどにインストールする
- PhoneGapBuild上でアプリの設定画面からデバッグ画面を開く
- エミュレータ上でインストールしたアプリを開始する
- PhoneGapBuildのデバッガからエミュレータが認識されてConcoleなどが利用できるようになる
- アプリの画面上に表示されているClickボタンをタップすると、Consoleにメッセージが追加される(webOS上ではアプリ開始後に、画面をタップしないとボタンが表示されないことがある)